/*
	Shorthand installing dependencies
		npm init -y
		npm install express mongoose
*/

					const express = require("express");

// this lets us use the mongoose module

					const mongoose = require("mongoose");

					const app = express();
					const port = 3000;

// 					SECTION - MongoDB connection
/*
					SYNTAX
						mongoose.connect("<MongoDB atlas connection string>", { useNewUrlParser : true, useUnifiedTopology : true} );
*/
// Connect to the data by passing in your connection string (from MongoDB)
// ".connect()" lets us connect and access the database that is sent to it via string
// b190-to-do is the database that we have created in our MongoDB
// this will not prevent Mongoose from being used in application and sending unneccessary warnings when we send requests

					mongoose.connect("mongodb+srv://giolimcaoco:Yx6WOVh2BqmAfb7y@wdc028-course-booking.bczl9fp.mongodb.net/b190-to-do?retryWrites=true&w=majority",
						{
							useNewUrlParser: true, 
							useUnifiedTopology: true 
						}
					);

// sets notification for connection success or failure
// allows us to handle errors when initial connection is established
// works with on and once mongoose methods

					let db = mongoose.connection;

// if a connection error occured, we will be seeing it in the console.
// console.error.bind allows us to print errors in the browser as well as in the terminal

					db.on("error", console.error.bind(console, "Connection Error"));

// if the connection is successful, confirm it in the console

					db.once("open", () => console.log("We're connected to the database"));

// Schema () is a method inside the mongoose module that lets us create schema for our database it receives an object with properties and data types that each property should have
// the "name" property should receive a string data type. (the data type should be written in Sentence case)
// the default property allows the server to automatically assign a value "pending" to the property if the user did not provide a value
					
					const taskSchema = new mongoose.Schema({
						name: String,
						status: {
							type: String, 
							default: "pending" 
						} 
					});

// 					SECTION - Models

// Models in mongoose use schemas and are used to complete the object instantiation that correspond to that schema
// 	Models use Schema and they act as the middleman from the server to the database
// First parameter is the collection in where to store the data
// Second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
// Models must be written in singular form, sentence case
// the "Task" variable now will be used for us to run commands for interacting with our database

					const Task = mongoose.model("Task", taskSchema);

// allows handling of json data strucutre

					app.use(express.json());

// receives all kinds of data

					app.use(express.urlencoded({extended: true}));

// Create a new task
/*
	BUSINESS LOGIC
		- add a functionality that will check if there are duplicate tasks
			- if the task already exists, we return an error
			- if the task does not exist, add it in the database
		- the task should be sent from the request body
		- create a new task object with a "name" field/property
		- "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

					app.post("/tasks", (req, res) => {
// findOne is the mongoose method for finding documents in the database. it works similar to .find in Robo 3T/MongoDB discussions
// findOne returns the first document it finds, if there are no documents, it will return "null"
// err parameter is for if there are errors that will be encountered during the findOne of the documents
// result is for if findOne is successful (whether are documents or none)
						Task.findOne( { name: req.body.name }, ( err, result ) => {
							if ( result !== null && result.name === req.body.name ) {
								return res.send ("Duplicate task found");
							} else {
// detects where to get the properties that will be needed to create the new "Task" object to be sotred in the database
// the code states that the "name" property will come from the request.body
								let newTask = new Task({
									name : req.body.name
								});
// .save() is a method to save the object created in the database . similar to .push()
								newTask.save( ( saveErr, savedTask ) => {
									if ( saveErr ){
										return console.error ( saveErr );
									} else {
										return res.status (201).send ("New task created");
									};
								});
							};
						});
					});

					app.get ("/tasks", (req, res) => {
						Task.find ( { }, (err, result) => {
							if (err) {
								return console.log (err);
							} else {
								return res.status(200).json ( result  );
								// return res.status(200).json ( { data : result } ); - creating a new object with "data" property and the value of the result
							}
						})
					})

					/*app.get("/tasks",(req,res)=>{
						Task.find({}, (err,result) => {
							if(result === null){
								return res.send("Error")
							}else{
								res.send(result)
							}
						})
					})*/

//----------------------------------------ACTIVITY------------------------------------
/*
					SECTION - Activity

	Business logic for registering a new user
		- add a functionality to check if there are duplicate users
			- if the user is already existing, return an error
			- if the user is NOT existing, save/add the user in the database
		- the user will be coming from the request body
		- create a new User object with a "username" and "password" fields/properties 
*/
/*
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a git repository named S35.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	7. Add the link in Boodle.
*/
					const userSchema = new mongoose.Schema({
						username: String,
						password: String
					});
					const User = mongoose.model("User", userSchema);

					app.post("/signup", (req, res) => {
						Task.findOne( { username: req.body.username }, ( err, result ) => {
							if ( result !== null && result.username === req.body.username ) {
								return res.send ("Duplicate user found");
							} else {
								let newUser = new User({
									username : req.body.username,
									password : req.body.password
								});

								newUser.save( ( saveErr, savedTask ) => {
									if ( saveErr ){
										return console.error ( saveErr );
									} else {
										return res.status (201).send ("New user registered");
									};
								});
							};
						});
					});






//-------------------------------------END of ACTIVITY--------------------------------




					app.listen(port, () => console.log(`Server running at port: ${port}`));


